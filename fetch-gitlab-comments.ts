const LIMIT = 100;
const WAIT_MS = 5000;
const TOKEN = Deno.env.get("GITLAB_TOKEN") || "";

type UserEvent = {
  action_name: string,
  target_title: string,
  author_username: string,
  note: {
    id: number,
    type: string,
    body: string,
    created_at: string,
    noteable_id: number,
    noteable_type: string,
    noteable_iid: number,
  }
};

type PaginatedResponse<T> = {
  nextPage: number,
  data: T,
};

const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

const writeToConsole = (message: string) => {
  Deno.writeAll(Deno.stdout, new TextEncoder().encode(message));
};

const getUserEventsApiUrl = (username: string, page = 1) => {
  return `https://gitlab.com/api/v4/users/${username}/events?action=commented&per_page=${LIMIT}&page=${page}`;
};

const fetchUserEvents = async (username: string, page = 1): Promise<PaginatedResponse<UserEvent[]>> => {
  const apiUrl = getUserEventsApiUrl(username, page);
  console.error(`Fetching ${apiUrl}`);

  const response = await fetch(apiUrl, { headers: { "Accept": "application/json", "PRIVATE-TOKEN": TOKEN } });

  const nextPageHeader = response.headers.get('x-next-page');
  const nextPage = nextPageHeader ? Number(nextPageHeader) : 0;

  const data = await response.json() as UserEvent[];

  return {
    nextPage,
    data,
  };
};

const main = async ([username]: string[]) => {
  if (!username) {
    console.error('Please provide a username to search for.');
    Deno.exit(1);
    return;
  }
  
  writeToConsole('[');
  for(let page = 1;page > 0;) {
    const response = await fetchUserEvents(username, page);

    if (page > 1) {
      writeToConsole(',')
    }

    page = response.nextPage;
    const result = response.data.map(x => JSON.stringify(x)).join(',');

    writeToConsole(result);

    console.error(`Sleeping for ${WAIT_MS}ms...`);
    await sleep(WAIT_MS);
  }
  writeToConsole(']');
};

if (import.meta.main) {
  await main(Deno.args);
}
