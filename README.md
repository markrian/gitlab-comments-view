## What do I do here?

Run something like this:

```
mkdir public
deno run --allow-net fetch-gitlab-comments.ts $MY_USERNAME > public/index.json

# Now it's time to mustachify
mustache public/index.json public/index.mustache > public/index.html

# Now link the main.js to public (you could also cp)
ln -s $PWD/main.js public/main.js
```

Then open `public/index.html` in your favorite browser and enjoy :)

![screenshot](./doc/screenshot.png)
